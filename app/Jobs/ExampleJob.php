<?php

namespace App\Jobs;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class ExampleJob extends Job
{
    private $name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug("job actived at: ".Carbon::now());
        Log::info($this->name);
    }
}
