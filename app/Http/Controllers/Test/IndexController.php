<?php

namespace App\Http\Controllers\Test;

use App\Jobs\ExampleJob;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller;
use Laravel\Lumen\Routing\Router;

class IndexController extends Controller {

    public function version (Router $router) {
        return $router->app->version();
//        return "5555";
    }

    public function json () {
        return response()->json(["name"=>"方海亮","age"=>29]);
    }

    public function users () {
        $userListKey = 'user_list_key';
        $userList = Cache::get($userListKey);
        if ($userList == false){
            $userList = DB::table("use")->select(['id', 'name'])->get();
            Cache::put($userListKey, $userList, 60);
        }
        return response()->json($userList);
    }

    public function jobs () {
        $this->dispatch(new ExampleJob("fanghailiang"));
        return "ok";
    }

    public function config () {
        return conf("servers.chaos");
    }

    public function error () {
        $a = 2/0;
        return 'ok';
    }
}
