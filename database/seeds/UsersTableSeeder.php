<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('use')->insert([
            'name' => "famg1",
            'age' => 22,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('use')->insert([
            'name' => "famg2",
            'age' => 24,
            'created_at' => date("Y-m-d H:i:s")
        ]);
    }
}
