<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVotesToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('use', function (Blueprint $table) {
            $table->addColumn("string", "name", [
                "length"=>20,
                "after"=>"id"
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('use', function (Blueprint $table) {
            $table->dropColumn(["name"]);
        });
    }
}
